\documentclass[a4paper]{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{1.0}
\begin{document}

\begin{Name}{1}{subcom}{Random Crew}{subcommand construction tool}{subcom reference page}

    \Prog{subcom}
    – tool to manage subcommands of your executable.


\section{Synopsis}

    \Prog{subcom}
        \oOpt{-a \Bar -w \Bar -s}
        \oOpt{-p path}
        \oOpt{-d \Bar -n}
        \Arg{script}
        \Arg{arguments}
    \\
    \Prog{subcom}
        \Opt{--help}\Bar\Opt{--version}


\section{Description}

    \Prog{subcom} is the tool to determine subcommands from arguments of your executable file.

    Subcommand is an executable file, found at locations, relative to the main script.
    The main script is first non-option argument of \Prog{subcom}.
    The name of subcommand and main script must consist of letters of latin alphabet (\Arg{a-z, A-Z}),
    digits, underscore symbol (\Arg{\_}) or dash symbol (\Arg{-}). Names cannot start with dash symbol.

    For example, you have script \Prog{foo} and its subcommand \Prog{bar}, located at \File{foo-bar}.
    The invocation of
    \begin{verbatim}
    $ subcom foo bar
    \end{verbatim}
    will print absolute path of \Arg{foo-bar} executable file: \File{/usr/bin/foo-bar}

    Remaining command line arguments, which do not represent subcommand, will be omitted:
    \begin{verbatim}
    $ subcom foo bar la la hey 1 2 3
    \end{verbatim}
    The result will be:
    \begin{verbatim}
    /usr/bin/foo-bar
    \end{verbatim}
    You may list remaining arguments with \Opt{-a} option, see it below.

\subsection{Locations}

    There are several types of locations to search for subcommands.
    \Prog{subcom} checks paths by two schemes: dash-delimeted words and nested directories.

    Dash-delimited files are checked in two locations as follows.
    \begin{itemize}
        \item[] One location is the directory with executable file.
        \begin{description}
            \item[foo bar] \File{/usr/bin/foo-bar}
            \item[foo bar more] \File{/usr/bin/foo-bar-more}
            \item[/usr/local/bin/foo bar] \File{/usr/local/bin/foo-bar}
        \end{description}
        Example above demonstrates search in the same directory, which contains original \File{foo}.

        \item[] Another location is in the directory \File{../lib/}\Arg{foo}\File{/}, relative to
        original executable \File{foo}.
        \begin{description}
            \item[foo bar] \File{/usr/lib/foo/foo-bar}
            \item[foo bar more] \File{/usr/lib/foo/foo-bar-more}
            \item[/usr/local/bin/foo bar] \File{/usr/local/lib/foo/foo-bar}
        \end{description}
    \end{itemize}

    Nested directories are checked like this:
    \begin{description}
        \item[foo bar] \File{/usr/bin/foo} and \File{/usr/libexec/foo/bar}
        \item[foo bar more] \File{/usr/bin/foo} and \File{/usr/libexec/foo/bar/more}
        \item[/usr/local/bin/foo bar] \File{/usr/local/libexec/foo/bar}
    \end{description}
    In the example above the location of subcommands \File{/usr/libexec/foo/} is calculated
    relatively to the location of main executable \File{/usr/bin/foo}.
    The same scheme keeps working with another prefixes instead of \File{/usr} above,
    like \File{/usr/local} or any other.

    The scheme with nested subdirectories has specific case for the launch:
    \begin{verbatim}
subcom foo bar lala
    \end{verbatim}
    When \Arg{lala} is not correct subcommand, but there is nested subdirectory \Arg{bar},
    the following file will be checked up as matching subcommand as well:
    \begin{verbatim}
/usr/libexec/foo/bar/bar
    \end{verbatim}

    As shown above, the location of subcommands is bound to the location of executable script
    (first argument \Arg{foo} in examples above) by default. The location to search for
    subcommands can be changed with the option \Opt{-p}.

    The scheme with dashes has higher priority over nested directories. If subcommand with dashes is found,
    contents of nested directories are ignored.

    If no matching subcommands are found, or no correct main script is given,
    \Prog{subcom} prints nothing to output. So one may check, whether some subcommand was found:
    \begin{verbatim}
test -n "$(subcom foo bar lala)"
    \end{verbatim}

\subsection{Generic usage example}

    The generic scheme to use \Prog{subcom} in some launcher script may look like this:
    \begin{verbatim}
command="$(subcom "$0" "$@")"
if test -z "${command}" ; then
    # handle the absence of subcommand
    exit
fi
for arg in $(subcom -s "$0" "$@") ; do
    shift
done

$command "$@"
    \end{verbatim}

    The first string is used to get the possible executable name.
    \begin{verbatim}
command="$(subcom "$0" "$@")"
    \end{verbatim}
    \Arg{\$0} here holds the executable name of main launcher script.
    \Arg{\$@} contains the argument string.

    The next block checks if some subcommand has been found.
    \begin{verbatim}
if test -z "${command}" ; then
    # handle the absence of subcommand
    exit
fi
    \end{verbatim}
    If \Prog{subcom} hasn't found an executable, matching anything in command string,
    it will be silent and \Arg{command} variable will be empty.

    The next block gets rid off the arguments in the initial command string, which decode the subcommand.
    The information about subcommand is stored in variable \Arg{command} after the code above.
    \begin{verbatim}
for arg in $(subcom -s "$0" "$@") ; do
    shift
done
    \end{verbatim}

    Finally, we launch found subcommand with all remaining arguments:
    \begin{verbatim}
$command "$@"
    \end{verbatim}


\section{Options}

    Options \Opt{-a}, \Opt{-w} and \Opt{-s} are mutually exclusive. If given at the same time,
    only the most right one in cmd will take effect.

    Options \Opt{-d} and \Opt{-n} are mutually exclusive too, as described above.

    \begin{description}
        \item[ \Opt{-a} ]
            Print all arguments, remaining after the found executable subcommand.
            Arguments will be printed by one at a string like this:
            \begin{verbatim}
$ subcom foo bar la la hey
            \end{verbatim}
            With the output:
            \begin{verbatim}
/usr/bin/foo-bar
la
la
hey
            \end{verbatim}
            \medbreak
            That sort of output allows to handle correctly arguments with spaces inside:
            \begin{verbatim}
$ subcom foo bar "la la" hey
            \end{verbatim}
            According output:
            \begin{verbatim}
/usr/bin/foo-bar
la la
hey
            \end{verbatim}

        \item[ \Opt{-w} ]
            Print only the remaining arguments after the executable subcommand has been found.

        \item[ \Opt{-s} ]
            Print only the arguments, according to found executable subcommand.
            The arguments are in one line and delimited with spaces.
            \medbreak
            The option is used to skip the arguments, which encode the subcommand, in a cycle.
            That can be done like this:
            \begin{verbatim}
for arg in $(subcom -s fooname "$@") ; do
    shift
done
            \end{verbatim}
            After that the command string in \Arg{\$@} will not contain anything about subcommand executable.

        \item[ \Opt{-p} \Arg{dir} ]
            Set directory \Arg{dir} as the path to search for subcommands.
            \medbreak
            For the script \File{/usr/bin/foo} and subcommand \File{/usr/bin/foo-bar}:
\begin{verbatim}
subcom -p /usr/bin foo bar
\end{verbatim}
            equals to default behaviour:
\begin{verbatim}
subcom foo bar
\end{verbatim}
            \medbreak
            For the script \File{/usr/bin/foo} and subcommand \File{/usr/libexec/foo/bar}:
\begin{verbatim}
subcom -p /usr/libexec foo bar
\end{verbatim}
            equals to default behaviour:
\begin{verbatim}
subcom foo bar
\end{verbatim}

        \item[ \Opt{-d} ]
            Use only scheme with dashes to search for subcommands:
            \File{foo-bar-more}.

        \item[ \Opt{-n} ]
            Use only scheme with nested subdirectories to search for subcommands:
            \File{foo/bar/more}.

        \item[ \Opt{--version} ]
            Print version and exit with 0 code.
        \item[ \Opt{--help} ]
            Print short help description and exit with 0 code.
    \end{description}


\section{Error codes}

    \begin{description}
        \item[ 1 ]
            Generic error.
        \item[ 2 ]
            No executable was given in command string.
        \item[ 3 ]
            Given main file is not executable.
        \item[ 4 ]
            Explicitly given path is not available.
    \end{description}

\section{See also}
    \Cmd{which}{1}


\section{Authors}
    Random Crew\\
%@% IF !embed %@%
    \URL{https://opendistro.org/subcom}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
