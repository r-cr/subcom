
   subcom – tool to manage subcommands of your executable.

Required

           shell (Bourne compatible)

Latest code

   subcom at buildbucket:
   https://bitbucket.org/r-cr/subcom

Description

   subcom is the tool to determine executable file from sentence-like string.
   That is used to manage subcommands of your script.

   The tool will parse the string:

     $ subcom script command

   and will find the executable file, according to command.

   subcom allows adding subcommands to some script without its modification,
   as the script parses command string and determines the subcommand in
   generic way.

Install

   To install subcom into dedicated directory $HOME/rrr/ (default):

     $ ./install.sh

   That is equal to:

     $ DESTDIR=${HOME}/rrr ./install.sh

   To install somewhere system-wide:

     $ DESTDIR=/usr/local ./install.sh

   Do not install directly to real system path like /usr with ./install.sh!
   Use separated directories for self-installed stuff. Install into working
   system with packages.

Distribution

   Available under ISC License.
   Copyright 2020 Random Crew.
   https://opendistro.org/subcom
