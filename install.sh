#!/bin/sh

INSTALL=${INSTALL:-"install"}
COPY=${COPY:-"cp -pRf"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-} 

# these can not be redefined.
pack_NAME="subcom"

# these can be redefined.
BINDIR="${PREFIX}/bin"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${pack_NAME}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"

if test -f config.sh ; then
    . config.sh
fi

install_all()
{
    ${MKDIR} \
        "${DESTDIR}${BINDIR}/" \
        "${DESTDIR}${DOCDIR}/" \
        "${DESTDIR}${MANDIR}/"

    ${INSTALL} -m755 bin/* "${DESTDIR}${BINDIR}/"
    ${COPY} doc/man/* "${DESTDIR}${MANDIR}/"
    ${COPY} doc/html/ "${DESTDIR}${DOCDIR}/"
    ${INSTALL} -m644 'README-subcom' 'License-ISC' "${DESTDIR}${DOCDIR}/"
}

set -o errexit
set -o xtrace

install_all "$@"
